<!DOCTYPE html>
<html>
<head>
	<title>Cookie's & Sessions</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
</head>
<body>

<?php
session_start();

// set the url of this document to _POST to.
$url = 'index.php';


// Save a cookie with the post value.
function saveCookie($name){
	$day = 30;  // Number of days to save your cookie.

	setcookie($name, $_POST[$name], time() + 86400 * $day);
}

// Check if the post is requested and create a cookie and a session var.
if(isset($_POST['getPost']) && count($_POST) > 0){
	foreach ($_POST as $key => $value) {
		if(isset($_POST[$key]) && $_POST[$key] !== '' && $key !== 'getPost'){
			$_SESSION[$key] = $_POST[$key] ;
			saveCookie($key);
		}	
	}
}

// Set the H1 title in the page.
if(isset($_SESSION['name'])){
	$title =  'Welcome ' . $_SESSION['name'];
}
elseif(isset($_COOKIE['name'])){
	$title = 'Welcome back '. $_COOKIE['name'];
}
else{
	$title =  'Welcome Stranger';
}

//	Destroy the session
if(isset($_POST['sessionDestroy'])){
	session_destroy();
	header('location: '. $url);
}

//	Destroy the cookies except the session cookie.
if(isset($_POST['cookieDestroy'])){

	foreach($_COOKIE as $key => $cookie){
		if($key !== 'PHPSESSID'){
			setcookie($key, '', time() - 3600);
		}
	}
	header('location: '. $url);
}

// Get the data from the cookie and put in into post if exists.
if (isset($_POST['getCookie'])){
	foreach ($_COOKIE as $key => $value) {
		if(isset($_COOKIE[$key])){
			$_POST[$key] = $_COOKIE[$key];
		}
	}
}

// Get the data from the session and put in into post if exists.
if (isset($_POST['getSession'])){
	foreach ($_SESSION as $key => $value) {
		if(isset($_SESSION[$key])){
			$_POST[$key] = $_SESSION[$key];
		}
	}
}


?>
<div class="container">
  <br><h1> <?= $title ?></h1><br>
	<div class="row"><hr>
		<div class="col-md-5">
			<form action='<?= $url ?>' method="POST">
				<div class="form-group">
				    <label for="name">Name</label>
				    <input type="text" class="form-control" name='name' id="name" value="<?php if(!empty($_POST['name'])) { echo $_POST['name']; } else{ echo '"placeholder="Enter your name"';}?> ">
				</div>				
				<div class="form-group">
				    <label for="email">Email address</label>
				    <input type="email" class="form-control" name='email' id="email" value="<?php if(!empty($_POST['email'])) { echo $_POST['email']; } else{ echo '"placeholder="Enter your email"';}?> ">
				</div>
				<div class="form-group">
				    <label for="message">Message</label>
				    <input type='message' class="form-control" name='message' value="<?php if(!empty($_POST['message'])){ echo $_POST['message']; }else{ echo '"placeholder="Message"';}?> ">
				</div>
				<input type="hidden" name="today" value="<?= time() ?>"><br>
				<input type="submit" name='getPost' class='btn btn-outline-info' value="Save me!">	
			</form>	
		</div>
		<div class="col-md-7">
			<table class="table table-hover">
			  <thead>
			    <tr>
			      <th>#</th>
			      <th>$_Post</th>
			      <th>$_SESSION</th>
			      <th>$_COOKIE</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">Name</th>
			      <td><?php if(isset($_POST['name'])) { echo $_POST['name']; } else { echo 'Not found'; }  ?></td>
			      <td><?php if(isset($_SESSION['name'])) { echo $_SESSION['name']; } else { echo 'Not found'; } ?></td>
			      <td><?php if(isset($_COOKIE['name'])) { echo $_COOKIE['name']; } else { echo 'Not found'; } ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Email</th>
			      <td><?php if(isset($_POST['email'])) { echo $_POST['email']; } else { echo 'Not found'; }  ?></td>
			      <td><?php if(isset($_SESSION['email'])) { echo $_SESSION['email']; } else { echo 'Not found'; } ?></td>
			      <td><?php if(isset($_COOKIE['email'])) { echo $_COOKIE['email']; } else { echo 'Not found'; } ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Message</th>
			      <td><?php if(isset($_POST['message'])) { echo $_POST['message']; } else { echo 'Not found'; }  ?></td>
			      <td><?php if(isset($_SESSION['message'])) { echo $_SESSION['message']; } else { echo 'Not found'; } ?></td>
			      <td><?php if(isset($_COOKIE['message'])) { echo $_COOKIE['message']; } else { echo 'Not found'; } ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Time</th>
			      <td><?php if(isset($_POST['today'])) { echo date('d-M-Y', $_POST['today']); } else { echo 'Not found'; }  ?></td>
			      <td><?php if(isset($_SESSION['today'])) { echo date('d-M-Y', $_SESSION['today']); } else { echo 'Not found'; } ?></td>
			      <td><?php if(isset($_COOKIE['today'])) { echo date('d-M-Y' , $_COOKIE['today']); } else { echo 'Not found'; } ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Clear</th>
			      <td>
		      		<form action='<?= $url ?>' method="POST">
		      			<input type="submit" name='refresh' class='btn btn-outline-danger' value="Clear"></td>
		      		</form>
			      <td>
			      	<form action='<?= $url ?>' method="POST">
			      		<input type="submit" name='sessionDestroy' class='btn btn-outline-danger' value="Clear">
			      	</form>
			      </td>
			      <td>
					<form action='<?= $url ?>' method="POST">
			      		<input type="submit" name='cookieDestroy' class='btn btn-outline-danger' value="Clear">
			      	</form>
			      </td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</div>
	<hr>
	<form action='<?= $url ?>' method="POST">
		<input type="submit" name='getCookie' class='btn btn-outline-success' value="Get from Cookie">	
			<form action='<?= $url ?>' method="POST">
				<input type="submit" name='getSession' class='btn btn-outline-success' value="Get from Session">	
			</form>
	</form>
</div>
</body>
</html>		